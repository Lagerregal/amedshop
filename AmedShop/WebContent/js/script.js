$(document).ready(function() {
	//default setup
	$.ajaxSetup({ scriptCharset: "utf-8" ,contentType: "application/x-www-form-urlencoded; charset=UTF-8" });
	
	//selectpicker
	$('.selectpicker').selectpicker();
	
	//fancybox
	$('.fancybox').fancybox();
	
	//product list isotope
	$('.isotope-container').isotope();
	$('.isotope-filters select').change(function(){
		var selector = "";
		$('.isotope-filters select').each(function(i, el){
			selector += $(el).val();
		});
		$('.isotope-container').isotope({ filter: selector });
	});
	
	
	//product images
	$(".product_image").elevateZoom({gallery:'product_images', cursor: 'pointer', galleryActiveClass: 'active', imageCrossfade: false, loadingIcon: 'http://www.elevateweb.co.uk/spinner.gif'}); 
	//pass the images to Fancybox
	$(".product_image").bind("click", function(e) {  
	  var ez =   $('.product_image').data('elevateZoom');	
		$.fancybox(ez.getGalleryList());
	  return false;
	});
	
	
	
	//search
	$(".product_search").autocomplete({
      source: "IndexController?event=search",
      minLength: 1,
      select: function( event, ui ) {
    	  self.location.href = "IndexController?event=product&uid="+ui.item.id;
      }
    }).data("ui-autocomplete")._renderItem = function( ul, item ) {
		return $( "<li></li>" )
		.data( "item.autocomplete", item )
		.append( "<a>" + (item.img?("<img src='images/products/" + item.img + "' style='max-width: 50px;'>"):"") + item.label +"</a>" )
		.appendTo( ul );
	};
	
	
	//refresh cart amount
	$('input.cart_amount').change(function(){
		var priceTotal = 0.0;
		$('.cart-item').each(function(e){
			priceTotal += $(this).find('.item-price').html() * $(this).find('.cart_amount').val();
		});
		$('.dynamic-total-price').html(priceTotal.toFixed(2).replace(/\./g, ",") + ' €');
		
		var el = $(this);
		$.ajax( "IndexController?event=cart&update=" + el.attr('data-invoiceItemIndex') + "&amount=" + el.val() )
		.done(function(data) {
			if(data == "ok"){
				$('.refresh-message').fadeOut().fadeIn();
				$('.refresh-message').fadeOut();
			}else{
				$('.refresh-message').fadeOut().fadeIn();
				$('.error-message').fadeOut();
				var product = jQuery.parseJSON(data);
				$('.error-message .cart_na_product_title').html(product.title);
				$('.error-message .cart_na_product_count').html(product.stockCount);
				$('.error-message').fadeIn();
			}
		});
	});
	
	
	//product reviews
	$('.show_review_form').click(function(){
		$('form.review_form').closest('.row').slideDown();
		$(this).hide();
	});
	
	
	
	//pw strength
	$('.pw-strength').keyup(function(){
		$('.result-strength').html(passwordStrength($('.pw-strength').val(),$('.email-strength').val()));
	});
	
	
	
	//configurator
	jQuery(document).ready(function(){

    	var yourDesigner = $('#clothing-designer').fancyProductDesigner({
    		editorMode: false,
    		fonts: ['Arial', 'Fearless', 'Helvetica', 'Times New Roman', 'Verdana', 'Geneva', 'Gorditas'],
    		customTextParameters: {colors: false, removable: true, resizable: true, draggable: true, rotatable: true, autoCenter: true, boundingBox: "Base"},
    		uploadedDesignsParameters: {draggable: true, removable: true, colors: '#000', autoCenter: true, boundingBox: "Base"}
    	}).data('fancy-product-designer');

    	//print button
		$('#print-button').click(function(){
			yourDesigner.print();
			return false;
		});

		//create an image
		$('#image-button').click(function(){
			yourDesigner.createImage();
			return false;
		});

		//create a pdf with jsPDF
		$('#pdf-button').click(function(){
			var image = new Image();
			image.src = yourDesigner.getProductDataURL('jpeg');
			image.onload = function() {
				var doc = new jsPDF();
				doc.addImage(this.src, 'JPEG', 0, 0, this.width * 0.2, this.height * 0.2);
				doc.save('Product.pdf');
			};
			return false;
		});

		//checkout button with getProduct()
		$('#checkout-button').click(function(){
			var product = yourDesigner.getProduct();
			console.log(product);
			return false;
		});

		//event handler when the price is changing
		$('#clothing-designer')
		.bind('priceChange', function(evt, price, currentPrice) {
			$('#thsirt-price').text(currentPrice);
		});

		//recreate button
		$('#recreation-button').click(function(){
			var fabricJSON = JSON.stringify(yourDesigner.getFabricJSON());
			$('#recreation-form input:first').val(fabricJSON).parent().submit();
			return false;
		});

		//click handler for input upload
		$('#upload-button').click(function(){
			$('#design-upload').click();
			return false;
		});

		//save image on webserver
		$('#save-image-php').click(function() {
			$.post( "/AmedShop/IndexController?event=configurator&save=image", {
				base64_image: yourDesigner.getProductDataURL(),
				price: yourDesigner.getPrice()
			}).done(function() {
				window.location.href = "IndexController?event=cart";
			});
		});

		//send image via mail
		$('#send-image-mail-php').click(function() {
			$.post( "php/send_image_via_mail.php", { base64_image: yourDesigner.getProductDataURL()} );
		});

		//upload image
		/*
		document.getElementById('design-upload').onchange = function (e) {
			if(window.FileReader) {
				var reader = new FileReader();
		    	reader.readAsDataURL(e.target.files[0]);
		    	reader.onload = function (e) {

		    		var image = new Image;
		    		image.src = e.target.result;
		    		image.onload = function() {
			    		var maxH = 400,
		    				maxW = 300,
		    				imageH = this.height,
		    				imageW = this.width,
		    				scaling = 1;

						if(imageW > imageH) {
							if(imageW > maxW) { scaling = maxW / imageW; }
						}
						else {
							if(imageH > maxH) { scaling = maxH / imageH; }
						}

			    		yourDesigner.addElement('image', e.target.result, 'my custom design', {colors: $('#colorizable').is(':checked') ? '#000000' : false, zChangeable: true, removable: true, draggable: true, resizable: true, rotatable: true, autoCenter: true, boundingBox: "Base", scale: scaling});
		    		};
				};
			}
			else {
				alert('FileReader API is not supported in your browser, please use Firefox, Safari, Chrome or IE10!');
			}
		};
		*/
    });
});