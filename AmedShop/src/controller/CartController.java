package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import domain.model.*;
import domain.repository.*;
import util.GeneralUtility;
import util.InvoiceUtility;
import view.Template;

/**
 * Servlet implementation class CartController
 */
@WebServlet("/CartController")
public class CartController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CartController() {
        super();
    }

    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			boolean doOutput = true;
			
			HttpSession session = (HttpSession) this.getServletContext().getAttribute("session");
			Invoice invoice = null;
			if(session.getAttribute("invoice") != null){
				invoice = (Invoice) session.getAttribute("invoice");
			}else{
				invoice = new Invoice();
				session.setAttribute("invoice", invoice);
			}
			
			
			// remove
			if(request.getParameter("remove") != null){
				invoice.getItems().remove(Integer.parseInt(request.getParameter("remove")));
			}
			
			// update
			if(request.getParameter("update") != null){
				invoice.getItems().get(Integer.parseInt(request.getParameter("update"))).setAmount(Integer.parseInt(request.getParameter("amount")));
				doOutput = false;
			}
			
			// add
			if(request.getParameter("add-product") != null){
				String productUid = request.getParameter("add-product");
				Product product = ProductRepository.getInstance().findByUid(productUid);
				
				//create invoice item
				InvoiceItem invoiceItem = convertProductToInvoiceItem(product);
				invoiceItem.setAmount(Integer.parseInt(request.getParameter("count")));
				
				//create invoice item options
				Map<String, String[]> map = request.getParameterMap();
				for( Entry<String, String[]> entry : map.entrySet() ){
					try{						
						if(entry.getKey().substring(0, 7).equals("option-")){
							ProductOption productOption = ProductOptionRepository.getInstance().findByUid(request.getParameter(entry.getKey()));
							InvoiceItemOption invoiceItemOption = new InvoiceItemOption();
							invoiceItemOption.setPrice(productOption.getPrice());
							invoiceItemOption.setTitle(productOption.getAttribute().getTitle() + ": " + productOption.getValue());
							invoiceItem.setPriceBase(invoiceItem.getPriceBase() + productOption.getPrice() );
							invoiceItem.getOptions().add(invoiceItemOption);
						}
					}catch(Exception e){}
				}
				
				invoice.getItems().add(invoiceItem);
			}
			
			Product productNotAvailable = getNotAvailableProduct(invoice);
			
			if(doOutput){
				Template template = GeneralUtility.getTemplate(this.getServletContext());
				template.setTitle("Warenkorb");
				template.getAssign().put("invoice", invoice);
				template.getAssign().put("notAvailable", productNotAvailable);
				template.getAssign().put("invoiceTotal", InvoiceUtility.calcTotalPrice(invoice));
			}else{
				PrintWriter out = response.getWriter();
				if(productNotAvailable == null){
					out.print("ok");
				}else{
					out.print("{" +
							"\"title\":\""+productNotAvailable.getTitle()+"\"," +
							"\"stockCount\":\""+productNotAvailable.getStockCount()+"\"" +
						"}");
				}
				out.close();
			}
			
		} catch (Exception e) {
			this.getServletContext().setAttribute("exception", e);
			GeneralUtility.forwardController(getServletContext(), request, response, "error");
		}
	}
	
	
	public static InvoiceItem convertProductToInvoiceItem(Product product){
		InvoiceItem invoiceItem = new InvoiceItem();
		invoiceItem.setPriceBase(product.getPriceBase());
		invoiceItem.setProduct(product.getUid());
		invoiceItem.setTitle(product.getTitle());
		return invoiceItem;
	}
	
	public static Product getNotAvailableProduct(Invoice invoice) throws Exception{
		Product notAvailable = null;
		for(InvoiceItem ii : invoice.getItems()){
			Product current = ProductRepository.getInstance().findByUid(ii.getProduct());
			if( current != null && current.getStockCount() < ii.getAmount() ){
				notAvailable = current;
				break;
			}
		}
		return notAvailable;
	}
}
