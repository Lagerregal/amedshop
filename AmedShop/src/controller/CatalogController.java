package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.model.*;
import domain.repository.*;
import util.GeneralUtility;
import view.Template;

/**
 * Servlet implementation class CatalogController
 */
@WebServlet("/CatalogController")
public class CatalogController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CatalogController() {
        super();
    }

    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			Template template = GeneralUtility.getTemplate(this.getServletContext());
			template.setTitle("Willkommen!");
			
			ArrayList<Product> products = null;
			if(request.getParameter("category") != null){
				template.getAssign().put("category", CategoryRepository.getInstance().findByUid(request.getParameter("category")));
				products = ProductRepository.getInstance().findByField("category", request.getParameter("category"));
				if(products.size() == 0){
					List<Product> productsList = new ArrayList<Product>(products);
					ArrayList<Category> subCatgeories = CategoryRepository.getInstance().findByField("parentCategory", request.getParameter("category"));
					for(Category category : subCatgeories){
						ArrayList<Product> newProducts = ProductRepository.getInstance().findByField("category", category.getUid() + "");
						List<Product> newProductsList = new ArrayList<Product>(newProducts);
						productsList.addAll(newProductsList);
					}
					products = (ArrayList<Product>) productsList;
				}
			}
			template.getAssign().put("products", products);
			
			
			//prepare filters for isotope
			if(products != null){
				HashMap<String, ArrayList<String>> filters = new HashMap<String, ArrayList<String>>();
				for(Product product : products){
					for(ProductAttribute pa : product.getAttributes()){
						if(!filters.containsKey(pa.getAttribute().getUid() + "")){
							filters.put(pa.getAttribute().getUid() + "", new ArrayList<String>());
						}
						if(!filters.get(pa.getAttribute().getUid() + "").contains(pa.getValue())){
							filters.get(pa.getAttribute().getUid() + "").add(pa.getValue());
						}
					}
				}
				template.getAssign().put("filters", filters);
			}
			HashMap<String, Attribute> attributes = new HashMap<String, Attribute>();
			for(Object a : AttributeRepository.getInstance().findAll()){
				attributes.put(((Attribute)a).getUid()+"", (Attribute)a);
			}
			template.getAssign().put("attributes", attributes);
			
			
		} catch (Exception e) {
			this.getServletContext().setAttribute("exception", e);
			GeneralUtility.forwardController(getServletContext(), request, response, "error");
		}
	}

}
