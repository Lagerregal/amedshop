package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import domain.model.*;
import util.GeneralUtility;
import view.Template;

/**
 * Servlet implementation class CheckoutController
 */
@WebServlet("/CheckoutController")
public class CheckoutController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CheckoutController() {
        super();
    }

    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			Template template = GeneralUtility.getTemplate(this.getServletContext());
			template.setTitle("Checkout");
			
			HttpSession session = (HttpSession) this.getServletContext().getAttribute("session");
			Invoice invoice = (Invoice) session.getAttribute("invoice");
			User user = GeneralUtility.getUser();
			
			if(user == null){
				session.setAttribute("goTo", "checkout");
				template.setTemplateFile("login");
				GeneralUtility.forwardController(getServletContext(), request, response, "login");
			}else{			
				template.getAssign().put("user", user);	
				template.getAssign().put("invoice", invoice);
			}
			
		} catch (Exception e) {
			this.getServletContext().setAttribute("exception", e);
			GeneralUtility.forwardController(getServletContext(), request, response, "error");
		}
	}

}
