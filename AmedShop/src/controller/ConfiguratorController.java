package controller;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;

import domain.model.Invoice;
import domain.model.InvoiceItem;
import domain.model.InvoiceItemOption;

import util.GeneralUtility;
import view.Template;

/**
 * Servlet implementation class ConfiguratorController
 */
@WebServlet("/ConfiguratorController")
public class ConfiguratorController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ConfiguratorController() {
        super();
    }

    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("unused")
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			if(request.getParameter("save") != null && request.getParameter("save").equals("image")){
				String fileName = System.currentTimeMillis()+".png";
				
			    try {
			    	String img64 = request.getParameter("base64_image").replace("data:image/png;base64,", "");
			        byte[] decodedBytes = DatatypeConverter.parseBase64Binary(img64);
			        BufferedImage bfi = ImageIO.read(new ByteArrayInputStream(decodedBytes));
			        File outputfile = new File(this.getServletContext().getRealPath("images/temp/"+fileName));
			        ImageIO.write(bfi , "png", outputfile);
			        bfi.flush();
			    } catch (Exception e) {
			        e.printStackTrace();
			    }
			    
			    
			    HttpSession session = (HttpSession) this.getServletContext().getAttribute("session");
				Invoice invoice = null;
				if(session.getAttribute("invoice") != null){
					invoice = (Invoice) session.getAttribute("invoice");
				}else{
					invoice = new Invoice();
					session.setAttribute("invoice", invoice);
				}
				
				InvoiceItem invoiceItem = new InvoiceItem();
				invoiceItem.setAmount(1);
				invoiceItem.setPriceBase(Double.parseDouble(request.getParameter("price")));
				invoiceItem.setProduct(-1);
				invoiceItem.setTitle("Eigenes Produkt");
				
				InvoiceItemOption iio = new InvoiceItemOption();
				iio.setPrice(0);
				iio.setSpecial("images/temp/"+fileName);
				iio.setTitle("Eigene Gestaltung");
				invoiceItem.getOptions().add(iio);
				
				invoice.getItems().add(invoiceItem);
				
				
				PrintWriter out = response.getWriter();
				out.print("ok");
				out.close();
			}else{
				Template template = GeneralUtility.getTemplate(this.getServletContext());
				//template.getAssign().put("products", products);
			}
			
		} catch (Exception e) {
			this.getServletContext().setAttribute("exception", e);
			GeneralUtility.forwardController(getServletContext(), request, response, "error");
		}
	}

}
