package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import util.GeneralUtility;

/**
 * Servlet implementation class ConfiguratorProductstageController
 */
@WebServlet("/ConfiguratorProductstageController")
public class ConfiguratorProductstageController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ConfiguratorProductstageController() {
        super();
    }

    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			response.getWriter().print("<section class=\"fpd-product-container fpd-border-color\"> 	<div class=\"fpd-menu-bar fpd-clearfix fpd-main-color\"> 		<h3>Nerd Konfigurator</h3> 		<!-- Menu --> 		<div class=\"fpd-menu\"> 			<ul class=\"fpd-clearfix\">" +
					"<li><span class=\"fpd-save-product fa fa-floppy-o fpd-main-color fpd-tooltip\" title=\"Produkt speichern\"></span></li>" +
					"<li><span class=\"fpd-download-image fa fa-cloud-download fpd-main-color fpd-tooltip\" title=\"Als Bild herunterladen\"></span></li>" +
					"<li><span class=\"fpd-save-pdf fa fa-file-o fpd-tooltip\" title=\"Als PDF herunterladen\"></span></li>" +
					"<li><span class=\"fpd-print fa fa-print fpd-tooltip\" title=\"Drucken\"></span></li>" +
					"<li><span class=\"fpd-reset-product fa fa-undo fpd-tooltip\" title=\"Zurücksetzen\"></span></li>" +
					"</ul> 		</div> 	</div> 	<!-- Kinetic Stage --> 	<div class=\"fpd-product-stage fpd-content-color\"> 		<canvas></canvas> 	</div> </section>");
			response.getWriter().close();
		} catch (Exception e) {
			this.getServletContext().setAttribute("exception", e);
			GeneralUtility.forwardController(getServletContext(), request, response, "error");
		}
	}

}
