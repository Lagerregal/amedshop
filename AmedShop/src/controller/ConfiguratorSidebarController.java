package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import util.GeneralUtility;

/**
 * Servlet implementation class ConfiguratorSidebarController
 */
@WebServlet("/ConfiguratorSidebarController")
public class ConfiguratorSidebarController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ConfiguratorSidebarController() {
        super();
    }

    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			//Template template = GeneralUtility.getTemplate(this.getServletContext());
			//template.getAssign().put(\"products\", products);
			
			response.getWriter().print("<section class=\"fpd-sidebar fpd-border-color fpd-clearfix\">" +
					"<!-- Navigation -->" +
					"<div class=\"fpd-navigation fpd-main-color\"><ul>" +
						"<li class=\"fa fa-book fpd-tooltip\" title=\"Produkte\" data-target=\".fpd-products\"></li>" +
						"<li class=\"fa fa-picture-o fpd-tooltip\" title=\"Design\" data-target=\".fpd-designs\"></li>" +
						"<li class=\"fa fa-font fpd-tooltip\" title=\"Eigenen Text einfügen\" data-target=\".fpd-custom-text\"></li>" +
						"<li class=\"fa fa-edit fpd-tooltip\" title=\"Elemente bearbeiten\" data-target=\".fpd-edit-elements\"></li>" +
						"<li class=\"fa fa-plus-square fpd-tooltip\" title=\"Eigenes Design hinzufügen\" data-target=\".fpd-upload-designs\"></li>" +
						"<li class=\"fa fa-facebook fpd-tooltip\" title=\"Fotos von Facebook hinzufügen\" data-target=\".fpd-fb-user-photos\"></li>" +
						"<li class=\"fa fa-hdd-o fpd-tooltip\" title=\"Produkt speichern\" data-target=\".fpd-saved-products\"></li>" +
					"</ul></div>" +
					"<!-- Content --> 	<div class=\"fpd-content fpd-content-color\">" +
					"<!-- Products --> 		<div class=\"fpd-products\"> 			<ul class=\"fpd-clearfix\"></ul> 		</div>" +
					"<!-- Designs --> 		<div class=\"fpd-designs\"> 			<ul class=\"fpd-clearfix\"></ul> 		</div>" +
					"<!-- Edit text --> 		<div class=\"fpd-custom-text\"> 			<h3>Text hinzufügen</h3> 			<textarea class=\"fpd-text-input fpd-textarea\">Geben Sie Ihren Text ein!</textarea> 			<button class=\"fpd-button-submit fpd-button fpd-submit \">Text hinzufügen</button> 		</div>" +
					"<!-- Edit elements --> 		<div class=\"fpd-edit-elements\"> 			<h3>Elemente bearbeiten</h3> 			<div> 				<select class=\"fpd-elements-dropdown\"> 					<option value=\"none\">Keine</option> 				</select> 			</div>" +
					"<!-- Toolbar --> 			<div class=\"fpd-toolbar\"> 				<div class=\"fpd-color-picker\"> 					<input type=\"text\" value=\"\"> 				</div> 				" +
					"<div class=\"fpd-fonts-dropdown-wrapper\"> 					<select class=\"fpd-fonts-dropdown\"></select> 				</div>" +
					"<div class=\"fpd-customize-text\"><textarea value=\"\" name=\"element_text\" class=\"fpd-text-input fpd-textarea fpd-active\"></textarea>" +
					"<div class=\"fpd-text-styles\"> 						<button title=\"Links ausrichten\" class=\"fpd-align-left fpd-button fpd-tooltip\"><span class=\"fa fa-align-left\"></span></button>" +
					"<button title=\"Mittig ausrichten\" class=\"fpd-align-center fpd-button fpd-tooltip\"><span class=\"fa fa-align-center\"></span></button>" +
					"<button title=\"Rechts ausrichten\" class=\"fpd-align-right fpd-button fpd-tooltip\"><span class=\"fa fa-align-right\"></span></button>" +
					"<button title=\"Fett\" class=\"fpd-bold fpd-button fpd-tooltip\"><span class=\"fa fa-bold\"></span></button>" +
					"<button title=\"Kursiv\" class=\"fpd-italic fpd-button fpd-tooltip\"><span class=\"fa fa-italic\"></span></button> 					</div> 				</div>" +
					"<div class=\"fpd-patterns-wrapper\"> 					<ul class=\"fpd-border-color\"></ul></div><div class=\"fpd-element-buttons\">" +
					"<button title=\"Horizontal zentrieren\" class=\"fpd-center-horizontal fpd-button fpd-tooltip\"><span class=\"fa fa-arrows-h\"></span></button>" +
					"<button title=\"Vertikal zentrieren\" class=\"fpd-center-vertical fpd-button fpd-tooltip\"><span class=\"fa fa-arrows-v\"></span></button>" +
					"<button title=\"Runter schieben\" class=\"fpd-move-down fpd-button fpd-tooltip\"><span class=\"fa fa-arrow-down\"></span></button>" +
					"<button title=\"Hoch schieben\" class=\"fpd-move-up fpd-button fpd-tooltip\"><span class=\"fa fa-arrow-up\"></span></button>" +
					"<button title=\"Zurücksetzen\" class=\"fpd-reset fpd-button fpd-tooltip\"><span class=\"fa fa-refresh\"></span></button>" +
					"<button title=\"Papierkorb\" class=\"fpd-trash fpd-button fpd-button-danger fpd-tooltip\"><span class=\"fa fa-trash-o\"></span></button></div></div></div>" +
					"<!-- Upload design --> 		<div class=\"fpd-upload-designs\"> 			<h3>Eigenes Design hochladen</h3><p>Lade dein eigenes Design hier hoch!</p>" +
					"<button class=\"fpd-button-submit fpd-button fpd-submit\">Upload</button>" +
					"<form class=\"fpd-upload-form\" style=\"display: none;\"> 				<input type=\"file\" class=\"fpd-input-design\" name=\"uploaded_file\"  /></form></div>" +
					"<!-- Facebook User Photos --> 		<div class=\"fpd-fb-user-photos\"> 			<h3><?php echo $fb_user_photos; ?></h3> 			<div> 				<div class=\"fpd-fb-loader fpd-clearfix\"> 					<div class=\"fb-login-button\" data-max-rows=\"1\" data-show-faces=\"false\" data-scope=\"user_photos,friends_photos\" autologoutlink=\"true\"></div> 					<span class=\"fpd-loading-gif\"></span> 				</div> 				<select class=\"fpd-fb-friends-select\" data-placeholder=\"<?php echo $fb_select_a_friend; ?>\"> 					<option value=\"\"></option> 				</select> 				<select class=\"fpd-fb-user-albums\" data-placeholder=\"<?php echo $fb_select_an_album; ?>\"> 					<option value=\"\"></option> 				</select> 			</div> 			<ul class=\"fpd-fb-user-photos-list fpd-border-color fpd-clearfix\"></ul> 		</div> " +
					"<!-- Saved products --> 		<div class=\"fpd-saved-products\"> 			<h3 class=\"fpd-border-color\">Gespeicherte Produkte</h3> 			<ul></ul> 		</div> 	</div>  </section>");
			response.getWriter().close();
		} catch (Exception e) {
			this.getServletContext().setAttribute("exception", e);
			GeneralUtility.forwardController(getServletContext(), request, response, "error");
		}
	}

}
