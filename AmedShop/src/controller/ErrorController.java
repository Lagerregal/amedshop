package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import util.GeneralUtility;
import view.Template;

/**
 * Servlet implementation class ErrorController
 */
@WebServlet("/ErrorController")
public class ErrorController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ErrorController() {
        super();
    }

    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Template template = GeneralUtility.getTemplate(this.getServletContext());
		template.setTitle("Sorry!");
		
		Exception e = (Exception) this.getServletContext().getAttribute("exception");
		try {
			template.appendToBody("<br /><br />Es ist da leider ein kleiner Fehler aufgetreten ;)<br /><br /><br />");
			if(e != null){
				template.appendToBody(e.toString());
				e.printStackTrace();
			}else{
				template.printHtml();
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}

}
