package controller;

import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import domain.model.*;
import domain.repository.InvoiceItemOptionRepository;
import domain.repository.InvoiceItemRepository;
import domain.repository.InvoiceRepository;
import domain.repository.ProductRepository;
import util.GeneralUtility;
import util.Mail;
import view.Template;

/**
 * Servlet implementation class FinisherController
 */
@WebServlet("/FinisherController")
public class FinisherController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FinisherController() {
        super();
    }

    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			Template template = GeneralUtility.getTemplate(this.getServletContext());
			template.setTitle("Finisher");
			
			HttpSession session = (HttpSession) this.getServletContext().getAttribute("session");
			Invoice invoice = (Invoice) session.getAttribute("invoice");
			User user = GeneralUtility.getUser();
			
			if(user != null && invoice != null && invoice.getItems().size() > 0){
				invoice.setFirstName(request.getParameter("firstName"));
				invoice.setLastName(request.getParameter("lastName"));
				invoice.setCountry(request.getParameter("country"));
				invoice.setCity(request.getParameter("city"));
				invoice.setZip(request.getParameter("zip"));
				invoice.setStreet(request.getParameter("street"));
				invoice.setShipping(request.getParameter("shipping"));
				invoice.setPayment(request.getParameter("payment"));
				invoice.setUser(user);
				invoice.setDateTime((int) (System.currentTimeMillis()/1000));
				
				int i = 0;
				Calendar cal = new GregorianCalendar();
				String numberBase = 
						String.valueOf(cal.get(Calendar.YEAR))
						+ String.valueOf(cal.get(Calendar.MONTH))
						+ String.valueOf(cal.get(Calendar.DAY_OF_MONTH))
						+ "-" + user.getUid() + "-";
				String number = "";
				Invoice existingInvoice = invoice;
				while(existingInvoice != null){
					i++;
					number = numberBase + i;
					existingInvoice = InvoiceRepository.getInstance().findOneByField("number", number);					
				}
				invoice.setNumber(number);
				
				//add all to db
				InvoiceRepository.getInstance().add(invoice);
				for(InvoiceItem ii : invoice.getItems()){
					
					//update stock count
					Product currentProduct = ProductRepository.getInstance().findByUid(ii.getProduct());
					if(currentProduct != null){
						currentProduct.setStockCount(currentProduct.getStockCount() - ii.getAmount());
						ProductRepository.getInstance().update(currentProduct);
					}
					
					ii.setInvoice(invoice.getUid());
					InvoiceItemRepository.getInstance().add(ii);
					for(InvoiceItemOption iio : ii.getOptions()){
						iio.setInvoiceItem(ii.getUid());
						InvoiceItemOptionRepository.getInstance().add(iio);
					}
				}
				
				template.appendToBody("Vielen Dank für Ihren Einkauf!<br /><br />Wir haben Ihnen die Rechnung per E-Mail zugesendet.<br />Alternativ können Sie die Rechnung nun hier <a target='_blank' href='InvoiceDownloadController?invoice="+invoice.getUid()+"'>herunterladen</a>.");
				session.setAttribute("invoice", null);
				
				template.getAssign().put("user", user);	
				template.getAssign().put("invoice", invoice);
				
				sendMail(invoice, user);
			}
			
		} catch (Exception e) {
			this.getServletContext().setAttribute("exception", e);
			GeneralUtility.forwardController(getServletContext(), request, response, "error");
		}
	}
	
	
	protected void sendMail(Invoice invoice, User user) throws Exception{
		//save attachment to temp file
		PDF pdf = new PDF();
		pdf.invoice = invoice;
		pdf.sc = this.getServletContext();
		String filePath = pdf.saveFileToTemp();
		
		//create mail
		Mail mail = new Mail();
		mail.setApiKey("fhsdhf3489hfe23489");
		mail.setTo(user.getEmail());
		mail.setFrom(user.getEmail());
		mail.setSubject("Ihre Rechnung Nr. "+ invoice.getNumber());
		mail.setMessage("Hallo " + user.getFirstName() + " " + user.getLastName() + ",<br /><br />"
						+ "vielen Dank für Ihren Einkauf im Nerd Shop!<br />"
						+ "Im Anhang finden Sie Ihre Rechnung.<br />"
						+ "<br />"
						+ "Mit freundlichen Grüßen<br />"
						+ "Ihr Nerd Shop Team");
		mail.setAttachmentFilePath(filePath);
		mail.setAttachmentFileName(invoice.getNumber()+".pdf");
		mail.setAttachmentFileType("application/pdf");
		mail.send();
		
		//delete temp file from OS
		java.io.File f = new java.io.File(filePath);
		f.delete();
	}

}
