package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import util.GeneralUtility;
import view.*;

/**
 * Servlet implementation class HelpController
 */
@WebServlet("/HelpController")
public class HelpController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HelpController() {
        super();
    }

    
	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			Template template = GeneralUtility.getTemplate(this.getServletContext());
			template.setTitle("Hilfe");
			template.appendToBody("Sie benötigen Hilfe? <br />Sie erreichen uns unter 0190 - 123 654<br />oder per E-Mail: <a href='mailto:info@nerd-shop.de'>mailto:info@nerd-shop.de</a>");
		} catch (Exception e) {
			this.getServletContext().setAttribute("exception", e);
			GeneralUtility.forwardController(getServletContext(), request, response, "error");
		}
	}

}
