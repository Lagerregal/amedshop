package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import util.GeneralUtility;
import view.*;

/**
 * Servlet implementation class ImpressumController
 */
@WebServlet("/ImpressumController")
public class ImpressumController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ImpressumController() {
        super();
    }

    
	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			Template template = GeneralUtility.getTemplate(this.getServletContext());
			template.setTitle("Impressum");
			template.appendToBody("Das hier ist das Impressum<br />"
					+ "&copy; 2014 by Markus Hölzle");
		} catch (Exception e) {
			this.getServletContext().setAttribute("exception", e);
			GeneralUtility.forwardController(getServletContext(), request, response, "error");
		}
	}

}
