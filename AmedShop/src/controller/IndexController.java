package controller;

import java.io.IOException;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import domain.model.User;

import util.*;
import view.*;

/**
 * 
 * 
 * Servlet implementation class IndexController
 */
@WebServlet("/IndexController")
public class IndexController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public IndexController() {
        super();
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			response.setCharacterEncoding("UTF-8");
			request.setCharacterEncoding("UTF-8");
			
			// Initialize template
			this.getServletContext().setAttribute("template", new Template(response, this.getServletContext()));
			
			// Init session
			HttpSession session = request.getSession(true);
			session.setAttribute("goTo", "");
			this.getServletContext().setAttribute("session", session);
			
			// Identify user
			User user = (User) session.getAttribute("user");
			GeneralUtility.setUser(user);
			
			// Find event
			String event = request.getParameter("event");
			if(event == null){
				event = "catalog";
			}
			
			// Init template
			Template template = GeneralUtility.getTemplate(this.getServletContext());
			template.setTemplateFile(event);
			
			// Forward to servlet controller
			GeneralUtility.forwardController(this.getServletContext(), request, response, event);
			
			// Print template
			template.printHtml();
			
		} catch (Exception e) {
			GeneralUtility.forwardController(getServletContext(), request, response, "error");
			this.getServletContext().setAttribute("exception", e);
		} 
	}

}
