package controller;

import java.awt.geom.Point2D;
import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.pdfclown.documents.Document;
import org.pdfclown.documents.Page;
import org.pdfclown.documents.contents.composition.PrimitiveComposer;
import org.pdfclown.documents.contents.fonts.StandardType1Font;
import org.pdfclown.files.File;
import org.pdfclown.files.SerializationModeEnum;
import org.pdfclown.samples.cli.Sample;

import domain.model.*;
import domain.repository.InvoiceRepository;
import util.GeneralUtility;
import view.PriceFormatDirective;

/**
 * Servlet implementation class InvoiceDownloadController
 */
@WebServlet("/InvoiceDownloadController")
public class InvoiceDownloadController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InvoiceDownloadController() {
        super();
    }

    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {			
			Invoice invoice = InvoiceRepository.getInstance().findByUid(request.getParameter("invoice"));
			User user = GeneralUtility.getUser();
			
			if(user != null && invoice != null && invoice.getUser().getUid() == user.getUid()){
				PDF pdf = new PDF();
				pdf.response = response;
				pdf.invoice = invoice;
				pdf.sc = getServletContext();
				pdf.run();
			}			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}


class PDF extends Sample{
	
	public HttpServletResponse response;
	public Invoice invoice;
	public ServletContext sc;

	  public void run(){
	    File file = new File();
	    Document document = file.getDocument();
	    populate(document);
	    response.setContentType("application/pdf");
	    try {
			file.save(
			        new org.pdfclown.bytes.OutputStream(
			          response.getOutputStream()
			          ),
			        SerializationModeEnum.Standard
			        );
			file.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	  }
	  
	  
	  public String saveFileToTemp() throws Exception{
		  String filePath = sc.getRealPath("temp") +"/"+ invoice.getNumber() +".pdf";
		  
		  File file = new File();
		    Document document = file.getDocument();
		    populate(document);
		    try {
				file.save(filePath, SerializationModeEnum.Standard);
				file.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		  return filePath;
	  }
	  

	  /**
	    Populates a PDF file with contents.
	  */
	  private void populate(Document document){
		int c = 0;
	    Page page = new Page(document);
	    document.getPages().add(page);
	    PrimitiveComposer composer = new PrimitiveComposer(page);
	    StandardType1Font font = new StandardType1Font(document,StandardType1Font.FamilyEnum.Helvetica,true,false);

	    //shop title
	    composer.setFont(font, 16);
	    composer.showText("Nerd Shop", new Point2D.Double(400,80) );
	    
	    //invoice header
	    c = 80;
	    composer.setFont(font, 10);
	    composer.showText(invoice.getFirstName() + " " + invoice.getLastName(), new Point2D.Double(60,c +12) );
	    composer.showText(invoice.getStreet(), new Point2D.Double(60,c +24) );
	    composer.showText(invoice.getZip() + " " + invoice.getCity(), new Point2D.Double(60,c +36) );
	    composer.showText(invoice.getCountry(), new Point2D.Double(60,c +48) );
	    
	    //invoice data
	    c = 120;
	    java.util.Date date = new java.util.Date((long)invoice.getDateTime()*1000);
	    GregorianCalendar cal = new GregorianCalendar();
	    cal.setTime(date);
	    composer.setFont(font, 8);
	    composer.showText("Datum: " + cal.get(Calendar.DAY_OF_MONTH)  + "." + (cal.get(Calendar.MONTH)+1) + "." + cal.get(Calendar.YEAR) , new Point2D.Double(400,c +12) );
	    composer.showText("Rechnung-Nr: " + invoice.getNumber(), new Point2D.Double(400,c +24) );
	    composer.showText("Zahlung: " + invoice.getPayment(), new Point2D.Double(400,c +36) );
	    composer.showText("Lieferung: " + invoice.getShipping(), new Point2D.Double(400,c +48) );
	    
	    //invoice title
	    composer.setFont(font, 16);
	    composer.showText("Rechnung Nr. " + invoice.getNumber(), new Point2D.Double(80, 230) );
	    
	    //items header
	    c = 270;
	    composer.setFont(font, 9);
    	composer.showText("Produkt", new Point2D.Double(80, c) );
    	composer.showText("Anzahl", new Point2D.Double(380, c) );
    	composer.showText("Preis", new Point2D.Double(450, c) );
    	composer.setLineWidth(20);
    	c += 30;
    	
	    //items
	    int i = 0;
	    double priceTotal = 0;
	    for(InvoiceItem ii : invoice.getItems()){
	    	int y = c+i*20;
	    	double price = ii.getPriceBase() * ii.getAmount();
	    	
	    	composer.setFont(font, 12);
	    	composer.showText(ii.getTitle(), new Point2D.Double(80,y) );
	    	composer.showText(ii.getAmount() + "", new Point2D.Double(380,y) );
	    	i++;
	    	
	    	for(InvoiceItemOption iio : ii.getOptions()){
	    		if(iio.getSpecial().equals("")){
		    		composer.setFont(font, 10);
		    		composer.showText(iio.getTitle() + "   (" + PriceFormatDirective.formatPrice(iio.getPrice()) + ")", new Point2D.Double(100,c+i*20) );
		    		i++;
	    		}else{
	    			composer.setFont(font, 10);
		    		composer.showText(iio.getTitle() + "   (Bild kann unter 'Mein Konto' angesehen werden)", new Point2D.Double(100,c+i*20) );
	    			i++;
	    		}
	    	}
	    	
	    	composer.setFont(font, 12);
	    	composer.showText( PriceFormatDirective.formatPrice(price) , new Point2D.Double(450,y) );
	    	
	    	priceTotal += price;
	    	c += 10;
	    }
	    
	    //price total
	    composer.setFont(font, 12);
    	composer.showText( "Gesamt:     " + PriceFormatDirective.formatPrice(priceTotal) , new Point2D.Double(380, 10+c+i*20) );
    	
    	//thank you text
    	composer.setFont(font, 10);
    	composer.showText( "Vielen Dank für Ihren Einkauf! Empfehlen Sie uns weiter!" , new Point2D.Double(80, 100+c+i*20) );

	    composer.flush();
	  }
	
}