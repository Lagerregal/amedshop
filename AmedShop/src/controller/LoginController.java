package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import domain.model.User;
import domain.repository.UserRepository;
import util.GeneralUtility;
import view.*;

/**
 * Servlet implementation class LoginController
 */
@WebServlet("/LoginController")
public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginController() {
        super();
    }

    
	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			Template template = GeneralUtility.getTemplate(this.getServletContext());
			template.setTitle("Login");
			
			if(GeneralUtility.getUser() != null){	// Forward if already logged in
				GeneralUtility.forwardController(getServletContext(), request, response, "catalog");
			}else{
				String email = request.getParameter("email");
				String password = request.getParameter("password");
				String error = "";
				
				User loginUser = null;
				if(email != null && password != null){
					loginUser = this.checkLogin(email, password);
					if(loginUser == null){
						error = "<br /><div class='alert alert-danger'>Benutzername oder Passwort sind nicht korrekt. Bitte versuchen Sie es erneut!</div><br />";
					}
				}
				
				if(loginUser == null){
					template.appendToBody(
							"<div class='col-md-6'>Hallo beim E-Shop 2.0!<br />" +
							"Bitte loggen Sie sich ein um einkaufen zu können!<br /><br />" +
							error +
							"<form method='post' action=''>" +
								"<label for='email'>E-Mail:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='email' name='email' id='email' class='form-control' placeholder='email@domain.com' required><br />" +
								"<label for='password'>Passwort:</label>&nbsp;<input type='password' name='password' id='password' class='form-control' placeholder='Passwort' required><br />" +
								"<input type='submit' class='btn btn-default' value='Login'>" +
							"</form></div>"
					);
				}else{
					// Update timestamp last login
					loginUser.setLastLoginDate(new java.sql.Timestamp((new java.util.Date()).getTime()));
					UserRepository.getInstance().update(loginUser);
					
					// Set session
					HttpSession session = (HttpSession) this.getServletContext().getAttribute("session");
					session.setAttribute("user", loginUser);
					GeneralUtility.setUser(loginUser);
					
					String goTo = (String) session.getAttribute("goTo");
					if(goTo == null || goTo.equals("")){
						goTo = "catalog";
					}
					template.setTemplateFile(goTo);
					GeneralUtility.forwardController(getServletContext(), request, response, goTo);
				}
			}
		} catch (Exception e) {
			this.getServletContext().setAttribute("exception", e);
			GeneralUtility.forwardController(getServletContext(), request, response, "error");
		}
	}
	
	
	/**
	 * checkLogin
	 * 
	 * @param email
	 * @param password
	 * @return
	 * @throws Exception 
	 */
	protected User checkLogin(String email, String password) throws Exception{
		User user = UserRepository.getInstance().findOneByField("email", email);
		
		if(user != null && !GeneralUtility.getHash(password).equals(user.getPassword())){
			user = null;
		}
		
		return user;
	}

}
