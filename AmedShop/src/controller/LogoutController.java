package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import util.GeneralUtility;
import view.*;

/**
 * Servlet implementation class RegisterController
 */
@WebServlet("/LogoutController")
public class LogoutController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LogoutController() {
        super();
    }

    
	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			Template template = GeneralUtility.getTemplate(this.getServletContext());
			template.setTitle("Logout");
			template.appendToBody("Auf Wiedersehen! Bis zum nächsten mal!");
			
			// Logout session
			HttpSession session = (HttpSession) this.getServletContext().getAttribute("session");
			session.setAttribute("user", null);
			session.invalidate();
			GeneralUtility.setUser(null);
			
		} catch (Exception e) {
			this.getServletContext().setAttribute("exception", e);
			GeneralUtility.forwardController(getServletContext(), request, response, "error");
		}
	}

}
