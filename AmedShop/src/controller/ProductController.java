package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.model.*;
import domain.repository.*;
import util.GeneralUtility;
import view.Template;

/**
 * Servlet implementation class ProductController
 */
@WebServlet("/ProductController")
public class ProductController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProductController() {
        super();
    }

    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			String productUid = request.getParameter("uid");
			
			//save review
			if(request.getParameter("rating") != null){
				ProductReview review = new ProductReview();
				review.setRating(Integer.parseInt(request.getParameter("rating")));
				review.setName(request.getParameter("name"));
				review.setReviewText(request.getParameter("review_text"));
				review.setProduct(Integer.parseInt(productUid));
				review.setDateTime((int) (System.currentTimeMillis()/1000));
				ProductReviewRepository.getInstance().add(review);
			}
			
			Product product = ProductRepository.getInstance().findByUid(Integer.parseInt(productUid));
			Template template = GeneralUtility.getTemplate(this.getServletContext());
			template.setTitle(product.getTitle());
			template.getAssign().put("product", product);
			int productReviewSum = 0;
			for(ProductReview pr : product.getReviews()){
				productReviewSum += pr.getRating();
			}
			if(product.getReviews().size() != 0){
				template.getAssign().put("product_avg", productReviewSum / product.getReviews().size());
			}
			
			//prepare product options for easy access
			HashMap<Integer, ArrayList<ProductOption>> options = new HashMap<Integer, ArrayList<ProductOption>>();
			for( ProductOption option: product.getOptions()){
				if(!options.containsKey(option.getAttribute().getUid())){
					options.put(option.getAttribute().getUid(), new ArrayList<ProductOption>());
				}
				options.get(option.getAttribute().getUid()).add(option);
			}
			template.getAssign().put("options", options.values() );
			
		} catch (Exception e) {
			this.getServletContext().setAttribute("exception", e);
			GeneralUtility.forwardController(getServletContext(), request, response, "error");
		}
	}

}
