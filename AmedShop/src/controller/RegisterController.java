package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.model.*;
import domain.repository.*;
import util.GeneralUtility;
import view.*;

/**
 * Servlet implementation class RegisterController
 */
@WebServlet("/RegisterController")
public class RegisterController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegisterController() {
        super();
    }

    
	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			Template template = GeneralUtility.getTemplate(this.getServletContext());
			template.setTitle("Registrieren");
			
			String email = request.getParameter("email");
			String password = request.getParameter("password");
			String error = "";

			boolean registerSuccess = false;
			if(email != null && password != null){
				
				if(!password.equals(request.getParameter("password_repeat"))){
					error = "<br /><div class='alert alert-danger'>Passwort Wiederholung war falsch! Bitte versuchen Sie es erneut!</div><br />";
				}else{
					registerSuccess = this.register(email, password, request);
					if(!registerSuccess){
						error = "<br /><div class='alert alert-danger'>Diese E-Mail Adresse wurde bereits registriert. Bitte versuchen Sie es erneut!</div><br />";
					}
				}
			}
			
			if(!registerSuccess){
				template.appendToBody(
					"<div class='col-md-6'>Hier können Sie sich kostenfrei registrieren:<br /><br />" +
					error +
					"<form method='post' action=''>" +
						"<label for='name'>Vorname:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='text' name='firstName' id='name' class='form-control' placeholder='Max' required><br />" +
						"<label for='name'>Nachname:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='text' name='lastName' class='form-control' placeholder='Mustermann' required><br />" +
						"<label for='name'>Land:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br /><select class='selectpicker' name='country'><option>Deutschland</option><option>Österreich</option><option>Schweiz</option><option>Frankreich</option></select><br />"+
						"<label for='name'>Stadt:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='text' name='city' class='form-control' placeholder='Stadt' required><br />" +
						"<label for='name'>PLZ:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='number' min='1' max='99999' name='zip' class='form-control' placeholder='PLZ' required><br />" +
						"<label for='name'>Straße und Hausnummer:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='text' name='street' class='form-control' placeholder='Straße' required><br />" +
						"<label for='email'>E-Mail:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type='email' name='email' id='email' class='form-control email-strength' placeholder='email@domain.com' required><br />" +
						"<label for='password'>Passwort:</label>&nbsp;<input type='password' name='password' id='password' class='form-control pw-strength' placeholder='Passwort' required><span class='result-strength' style='color: #00CC00;'></span><br /><br />" +
						"<label for='password'>Passwort wiederholen:</label>&nbsp;<input type='password' name='password_repeat' id='password_repeat' class='form-control' placeholder='Passwort Wiederholung' required><br />" +
						"<input type='submit' class='btn btn-default' value='registrieren'>" +
					"</form></div>"
				);
			}else{
				template.appendToBody("Vielen Dank für Ihre Registierung! Sie können Sich nun einloggen.");
			}
		} catch (Exception e) {
			this.getServletContext().setAttribute("exception", e);
			GeneralUtility.forwardController(getServletContext(), request, response, "error");
		}
	}
	
	
	/**
	 * register
	 * 
	 * @param email
	 * @param password
	 * @param name
	 * @return
	 * @throws Exception
	 */
	protected boolean register(String email, String password, HttpServletRequest request) throws Exception{
		boolean registerSuccess = false;
		
		User existingUser = UserRepository.getInstance().findOneByField("email", email);
		if(existingUser == null){
			
			//create new User
			User newUser = new User();
			newUser.setFirstName(request.getParameter("firstName"));
			newUser.setLastName(request.getParameter("lastName"));
			newUser.setCountry(request.getParameter("country"));
			newUser.setCity(request.getParameter("city"));
			newUser.setZip(request.getParameter("zip"));
			newUser.setStreet(request.getParameter("street"));
			newUser.setEmail(email);
			newUser.setPassword(GeneralUtility.getHash(password));
			UserRepository.getInstance().add(newUser);
			
			registerSuccess = true;
		}
		
		return registerSuccess;
	}

}
