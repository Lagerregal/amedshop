package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.*;

import domain.model.*;
import domain.repository.ProductRepository;
import util.GeneralUtility;

/**
 * Servlet implementation class SearchController
 */
@WebServlet("/SearchController")
public class SearchController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchController() {
        super();
    }

    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("unchecked")
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			//Template template = GeneralUtility.getTemplate(this.getServletContext());
			
			String q = request.getParameter("term")
					.replace("Ã¤", "ä")
					.replace("Ã¶", "ö")
					.replace("Ã¼", "ü")
					.replace("Ã", "ß")
					.trim();
			HashMap<String, String> hm = new HashMap<String, String>();
			hm.put("title", "%"+q+"%");
			hm.put("descriptionShort", "%"+q+"%");
			ArrayList<Product> resultProducts = ProductRepository.getInstance().searchInFields(hm);
			
			JSONArray jsonResult = new JSONArray();
			
			for(Product product : resultProducts){
				//result += "{"id":"Branta hrota","label":"Pale-bellied Brent Goose","value":"Pale-bellied Brent Goose"},";
				JSONObject obj = new JSONObject();
				obj.put("id", product.getUid());
				obj.put("label", product.getTitle());
				obj.put("value", product.getTitle());
				if(product.getImages().size() > 0){
					obj.put("img", product.getImages().get(0).getUrl());
				}
				jsonResult.add(obj);
			}
			
			response.setContentType("application/json;charset=UTF-8");
			PrintWriter out = response.getWriter();
			out.print(jsonResult);
			out.close();
		} catch (Exception e) {
			this.getServletContext().setAttribute("exception", e);
			GeneralUtility.forwardController(getServletContext(), request, response, "error");
		}
	}

}
