package core;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;


public abstract class AbstractRepository implements RepositoryInterface{
	protected static DbConnector connector;
	protected String className;
	
	private final static String dbPrefix = "shop";
	
	
	
	/**
	 * AbstractRepository
	 * 
	 * @throws Exception
	 */
	public AbstractRepository() throws Exception{
		connector = DbConnector.getInstance();
		className = getClass().getSimpleName().replace("Repository", "");
	}
	
	
	/**
	 * findOneByField
	 * 
	 * @param field
	 * @param value
	 * @return
	 * @throws Exception
	 */
	public <T extends Object> T findOneByField(String field, String value) throws Exception{	
		HashMap<String, String> fields = new HashMap<String, String>();
		fields.put(field, value);
		return findOneByMultipleFields(fields);
	}
	
	
	/**
	 * findOneByMultipleFields
	 * 
	 * @param fields
	 * @return
	 * @throws Exception
	 */
	public <T extends Object> T findOneByMultipleFields(HashMap<String, String> fields) throws Exception{
		return findOneByMultipleFields(fields, null);
	}
	
	
	/**
	 * findOneByMultipleFields
	 * 
	 * @param fields
	 * @param sorting
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public <T extends Object> T findOneByMultipleFields(HashMap<String, String> fields, String sorting) throws Exception{
		Class<?> classObject = Class.forName("domain.model."+className);
		Object resultObject = null;
		
		String where = "";
		Set<String> keys = fields.keySet();
		boolean isFirst = true;
		for (String singleKey : keys) {
			if(isFirst){
				isFirst = false;
			}else{
				where += " AND ";
			}
			where += "" + getTableFieldName(singleKey) + " = '" + fields.get(singleKey).replace("'", "\'") + "' ";
		}
		
		Statement smt = connector.getConn().createStatement();
		ResultSet rs = smt.executeQuery(
				"SELECT * " +
				"FROM "+ getTableName(className) +" " +
				"WHERE " + where +
				( (sorting == null)?(""):(" ORDER BY " + sorting + " ") ) +
				"LIMIT 1");
		if(rs.next()){
			resultObject = mapResultSetToModelObject(classObject, rs);
		}
		smt.close();
		
		return (T) classObject.cast( resultObject );
	}
	
	
	/**
	 * findOneByUid
	 * 
	 * @param value
	 * @return
	 * @throws Exception
	 */
	public <T extends Object> T findByUid(String value) throws Exception{
		return findOneByField("uid", value);
	}
	
	
	/**
	 * findByUid
	 * 
	 * @param value
	 * @return
	 * @throws Exception
	 */
	public <T extends Object> T findByUid(int value) throws Exception{
		return findByUid(String.valueOf(value));
	}
	
	
	/**
	 * findByField
	 * 
	 * @param field
	 * @param value
	 * @return
	 * @throws Exception
	 */
	public <T extends Object> ArrayList<T> findByField(String field, String value, String sorting) throws Exception{
		HashMap<String, String> fields = new HashMap<String, String>();
		fields.put(field, value);
		return findByMultipleFields(fields, sorting);
	}
	
	
	/**
	 * findByMultipleFields
	 * 
	 * @param fields
	 * @param sorting
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public <T extends Object> ArrayList<T> findByMultipleFields(HashMap<String, String> fields, String sorting) throws Exception{
		Class<?> classObject = Class.forName("domain.model."+className);
		ArrayList<T> resultsObjectsList = new ArrayList<T>();
		
		String where = "";
		Set<String> keys = fields.keySet();
		boolean isFirst = true;
		for (String singleKey : keys) {
			if(isFirst){
				isFirst = false;
			}else{
				where += " AND ";
			}
			where += "" + getTableFieldName(singleKey) + " = '" + fields.get(singleKey).replace("'", "\'") + "' ";
		}
		
		Statement smt = connector.getConn().createStatement();
		ResultSet rs = smt.executeQuery(
				"SELECT * " +
				"FROM "+ getTableName(className)+" " +
				"WHERE " + where +
				( (sorting == null)?(""):("ORDER BY "+sorting) )
				);
		while(rs.next()){
			resultsObjectsList.add((T) classObject.cast(mapResultSetToModelObject(classObject, rs)));
		}
		smt.close();
		
		return resultsObjectsList;
	}
	
	
	
	/**
	 * searchInFields
	 * 
	 * @param fields
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public <T extends Object> ArrayList<T> searchInFields(HashMap<String, String> fields) throws Exception{
		Class<?> classObject = Class.forName("domain.model."+className);
		ArrayList<T> resultsObjectsList = new ArrayList<T>();
		
		String where = "";
		Set<String> keys = fields.keySet();
		boolean isFirst = true;
		for (String singleKey : keys) {
			if(isFirst){
				isFirst = false;
			}else{
				where += " OR ";
			}
			where += "" + getTableFieldName(singleKey) + " LIKE '" + fields.get(singleKey).replace("'", "\'") + "' ";
		}
		
		Statement smt = connector.getConn().createStatement();
		ResultSet rs = smt.executeQuery(
				"SELECT * " +
				"FROM "+ getTableName(className)+" " +
				"WHERE " + where
				);
		while(rs.next()){
			resultsObjectsList.add((T) classObject.cast(mapResultSetToModelObject(classObject, rs)));
		}
		smt.close();
		
		return resultsObjectsList;
	}
	
	
	/**
	 * findByField
	 * 
	 * @param field
	 * @param value
	 * @return
	 * @throws Exception
	 */
	public <T extends Object> ArrayList<T> findByField(String field, String value) throws Exception{
		return findByField(field, value, null);
	}
	
	
	/**
	 * findAll
	 * 
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public <T extends Object> ArrayList<T> findAll() throws Exception{
		Class classObject = Class.forName("domain.model."+className);
		ArrayList<T> resultsObjectsList = new ArrayList<T>();
		
		Statement smt = connector.getConn().createStatement();
		ResultSet rs = smt.executeQuery(
				"SELECT * " +
				"FROM "+ getTableName(className));
		while(rs.next()){
			resultsObjectsList.add((T) classObject.cast(mapResultSetToModelObject(classObject, rs)));
		}
		smt.close();
		
		return resultsObjectsList;
	}
	
	
	/**
	 * add
	 * 
	 * @param object
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void add(Object object) throws Exception{
		Class classObject = Class.forName("domain.model."+className);
		StringBuffer insertString = new StringBuffer("INSERT INTO "+ getTableName(className) +" (");
		StringBuffer fieldsString = new StringBuffer();
		StringBuffer valueString = new StringBuffer();
		ArrayList values = new ArrayList();
		
		int j = 0;
		Field[] fields = classObject.getDeclaredFields();
		for(int i=0; i<fields.length; i++){
			if(!fields[i].getType().getSimpleName().equals("ArrayList")){
				String attrName = fields[i].getName();
				if(!attrName.equals("uid")){
					if(j != 0 ){
						fieldsString.append(", ");
						valueString.append(", ");
					}
					
					String fieldName = getTableFieldName(attrName);
					fieldsString.append(fieldName);
					Method getter = classObject.getMethod("get"+attrName.substring(0, 1).toUpperCase()+attrName.substring(1), new Class[]{});
					Object value = getter.invoke(object, new Object[]{});	//get current value
					
					//handle Maps
					if(fields[i].getType().getSimpleName().equals("Map")){
						try{
							XStream xStream = new XStream(new DomDriver());
							value = xStream.toXML(value);
						}catch(Exception e){
							value = new String();
						}
					}else if(AbstractModel.class.isAssignableFrom(fields[i].getType())){
						value = ((AbstractModel) value).getUid();
					}
					values.add(value);
					valueString.append("?");
					j++;
				}
			}
		}
		insertString.append(fieldsString.toString() + ") VALUES (" + valueString.toString() + ")");
		
		PreparedStatement smt = connector.getConn().prepareStatement(insertString.toString(), Statement.RETURN_GENERATED_KEYS);
		for(int i=0; i<values.size(); i++){
			smt.setObject((i+1), values.get(i));
		}
		smt.executeUpdate();	//execute insert
		
		//set new uid to object
		ResultSet rs = smt.getGeneratedKeys();
		rs.next();
		Method setUidMethod = classObject.getMethod("setUid", new Class[]{int.class});
		setUidMethod.invoke(object, new Object[]{rs.getInt(1)});
		smt.close();
	}
	
	
	/**
	 * update
	 * 
	 * @param object
	 * @throws Exception
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void update(Object object) throws Exception{
		Class classObject = Class.forName("domain.model."+className);
		StringBuffer updateString = new StringBuffer("UPDATE "+ getTableName(className) +" SET ");
		int objectUid = (int) classObject.getMethod("getUid", new Class[]{}).invoke(object, new Object[]{});
		ArrayList values = new ArrayList();
		
		Field[] fields = classObject.getDeclaredFields();
		int j = 0;
		for(int i=0; i<fields.length; i++){
			String attrName = fields[i].getName();
			if(!attrName.equals("uid")){
				Method getter = classObject.getMethod("get"+attrName.substring(0, 1).toUpperCase()+attrName.substring(1), new Class[]{});
				Object value = getter.invoke(object, new Object[]{});	//get current value
				
				//handle Maps
				if(fields[i].getType().getSimpleName().equals("Map")){
					try{
						XStream xStream = new XStream(new DomDriver());
						value = xStream.toXML(value);
					}catch(Exception e){
						value = new String();
					}
				}
				
				if(!value.getClass().getSimpleName().equals("ArrayList")){
					values.add(value);
					if( j != 0 ){
						updateString.append(", ");
					}
					updateString.append(getTableFieldName(attrName) + "=? ");
					j++;
				}
			}
		}
		
		updateString.append(" WHERE uid="+objectUid);
		
		PreparedStatement smt = connector.getConn().prepareStatement(updateString.toString());
		for(int i=0; i<values.size(); i++){
			smt.setObject((i+1), values.get(i));
		}
		smt.executeUpdate();	//execute insert
		smt.close();
	}
	
	
	
	
	
	
	/**********************************************/
	/***************** Helpers ********************/
	/**********************************************/
	
	
	/**
	 * mapResultSetToModelObject
	 * 
	 * @param classObject
	 * @param rs
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected static <T extends Object> T mapResultSetToModelObject(Class classObject, ResultSet rs) throws Exception{	
		T mappedObject = (T) classObject.newInstance();
		
		//try{	//debug
			
		Field[] fields = classObject.getDeclaredFields();
		HashMap<String, Class> fieldsMap = new HashMap<String, Class>();
		for(int i=0; i<fields.length; i++){
			Class currentFieldType = fields[i].getType();
		
			try{
				ParameterizedType listType = (ParameterizedType) fields[i].getGenericType();
				currentFieldType = (Class<?>) listType.getActualTypeArguments()[0];
			}catch(Exception e){}
			fieldsMap.put(fields[i].getName(), currentFieldType);
		}
			
		ResultSetMetaData rsm = rs.getMetaData();
		for(int i=0; i<rsm.getColumnCount(); i++){
			String columnName = rsm.getColumnName(i+1);
			String methodSet = "set" + getObjectAttributeName(columnName, true);
			String objectAttributeName = getObjectAttributeName(columnName, false);
			Class fieldType = fieldsMap.get(objectAttributeName);
			Object[] parameters = new Object[1];
			
			if(fieldType != null && fieldType.getSimpleName().equals("Map")){	//cast XML to Map
				String stringValue = rs.getString(i+1);
				
				if(!stringValue.equals("")){
					XStream xStream = new XStream(new DomDriver());
					parameters[0] = (Map<String,String>) xStream.fromXML(stringValue);
				}else{
					parameters[0] = null;
				}
			} else if(fieldType != null && AbstractModel.class.isAssignableFrom(fieldType) ){	//cast many (this object) to one relations
				String repositoryName = "domain.repository." + fieldType.getSimpleName() + "Repository";
				Method callRepositoryInstance = Class.forName(repositoryName).getMethod("getInstance", null);
				core.AbstractRepository objectRepository = (AbstractRepository) callRepositoryInstance.invoke(null, null);
				parameters[0] = objectRepository.findByUid(rs.getString(i+1));
			}else{	//cast everything else
				if(columnName.equals("uid")){
					fieldType = Integer.class;
				}
				parameters[0] = rs.getObject(i+1, fieldType);
			}
			fieldsMap.remove(objectAttributeName);
			
			java.beans.Statement exp = new java.beans.Statement(mappedObject, methodSet, parameters);
			exp.execute();
		}
		
		// map one (this object) to many relations
		for(Map.Entry<String, Class> objectField : fieldsMap.entrySet()){
			Object[] parameters = new Object[1];
			String repositoryName = "domain.repository." + objectField.getValue().getSimpleName() + "Repository";
			Method callRepositoryInstance = Class.forName(repositoryName).getMethod("getInstance", null);
			core.AbstractRepository objectRepository = (AbstractRepository) callRepositoryInstance.invoke(null, null);
			parameters[0] = objectRepository.findByField(getTableFieldName(classObject.getSimpleName()), ((AbstractModel) mappedObject).getUid()+"");

			java.beans.Statement exp = new java.beans.Statement(mappedObject, "set" + getObjectAttributeName(objectField.getKey(), true) , parameters);		
			exp.execute();
		}
		
		//}catch(Exception e){ e.printStackTrace(); }
		
		return (T) classObject.cast( mappedObject );
	}
	
	
	/**
	 * getTableName
	 * 
	 * @param className
	 * @return
	 */
	protected static String getTableName(String className){		
		char[] chars = className.toCharArray();
		StringBuffer attributeName = new StringBuffer();
		for(int i =0; i<chars.length; i++){
			if(Character.isUpperCase(chars[i])){
				chars[i] = Character.toLowerCase(chars[i]);
				attributeName.append('_');
			}
			attributeName.append(chars[i]);
		}
		return dbPrefix + attributeName.toString();
	}
	
	
	/**
	 * getObjectAttributeName
	 * 
	 * @param field
	 * @param firstUpper
	 * @return
	 */
	protected static String getObjectAttributeName(String field, boolean firstUpper){
		char[] fieldChars = field.toLowerCase().toCharArray();
		StringBuffer attributeName = new StringBuffer();
		if(firstUpper){
			fieldChars[0] = Character.toUpperCase(fieldChars[0]);
		}
		for(int i =0; i<fieldChars.length; i++){
			if(fieldChars[i] == '_'){
				fieldChars[i+1] = Character.toUpperCase(fieldChars[i+1]);
			}else{
				attributeName.append(fieldChars[i]);
			}
		}
		return attributeName.toString();
	}
	
	/**
	 * getTableFieldName
	 * 
	 * @param field
	 * @return
	 */
	protected static String getTableFieldName(String field){
		char[] chars = field.toCharArray();
		StringBuffer result = new StringBuffer();
		chars[0] = Character.toLowerCase(chars[0]);
		for(int i =0; i<chars.length; i++){
			if(Character.isUpperCase(chars[i])){
				chars[i] = Character.toLowerCase(chars[i]);
				result.append('_');
			}
			result.append(chars[i]);
		}
		return result.toString();
	}
	
}
