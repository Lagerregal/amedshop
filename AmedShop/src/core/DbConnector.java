package core;

import java.sql.DriverManager;

import com.mysql.jdbc.Connection;

public class DbConnector {

	private static DbConnector instance;
	private Connection conn;
	
	private final static String user = "ext_adv";
	private final static String password = "Lagerregal";
	
	private DbConnector() throws Exception{
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		conn = (Connection) DriverManager.getConnection("jdbc:mysql://lagerregal.dyndns.org/ext_adv?user="+user+"&password="+password);
		//conn.close();
	}
	
	public static DbConnector getInstance() throws Exception{
		if(DbConnector.instance == null){
			DbConnector.instance = new DbConnector();
		}
		return DbConnector.instance;
	}

	public Connection getConn() {
		return conn;
	}
	
}
