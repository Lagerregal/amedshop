package domain.model;

public class Attribute extends core.AbstractModel {

	protected String title;

	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
}
