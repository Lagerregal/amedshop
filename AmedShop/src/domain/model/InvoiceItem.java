package domain.model;

import java.util.ArrayList;

public class InvoiceItem extends core.AbstractModel {

	protected int invoice;
	protected int product;
	protected String title;
	protected int amount;
	protected double priceBase;
	protected ArrayList<InvoiceItemOption> options = new ArrayList<InvoiceItemOption>();
	
	
	public int getInvoice() {
		return invoice;
	}
	public void setInvoice(int invoice) {
		this.invoice = invoice;
	}
	public int getProduct() {
		return product;
	}
	public void setProduct(int product) {
		this.product = product;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public double getPriceBase() {
		return priceBase;
	}
	public void setPriceBase(double priceBase) {
		this.priceBase = priceBase;
	}
	public ArrayList<InvoiceItemOption> getOptions() {
		return options;
	}
	public void setOptions(ArrayList<InvoiceItemOption> options) {
		this.options = options;
	}
	
}
