package domain.model;

public class InvoiceItemOption extends core.AbstractModel {
	
	protected int invoiceItem;
	protected String title;
	protected double price;
	protected String special = "";
	
	
	public int getInvoiceItem() {
		return invoiceItem;
	}
	public void setInvoiceItem(int invoiceItem) {
		this.invoiceItem = invoiceItem;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getSpecial() {
		return special;
	}
	public void setSpecial(String special) {
		this.special = special;
	}
}
