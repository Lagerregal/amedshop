package domain.model;

import java.util.ArrayList;

public class Product extends core.AbstractModel {
	
	protected String title;
	protected String descriptionShort;
	protected double priceBase;
	protected int stockCount;
	protected int category;
	protected ArrayList<ProductAttribute> attributes;
	protected ArrayList<ProductOption> options;
	protected ArrayList<ProductImage> images;
	protected ArrayList<ProductReview> reviews;
	
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescriptionShort() {
		return descriptionShort;
	}
	public void setDescriptionShort(String descriptionShort) {
		this.descriptionShort = descriptionShort;
	}
	public double getPriceBase() {
		return priceBase;
	}
	public void setPriceBase(double priceBase) {
		this.priceBase = priceBase;
	}
	public int getStockCount() {
		return stockCount;
	}
	public void setStockCount(int stockCount) {
		this.stockCount = stockCount;
	}
	public int getCategory() {
		return category;
	}
	public void setCategory(int category) {
		this.category = category;
	}
	public ArrayList<ProductAttribute> getAttributes() {
		return attributes;
	}
	public void setAttributes(ArrayList<ProductAttribute> attributes) {
		this.attributes = attributes;
	}
	public ArrayList<ProductOption> getOptions() {
		return options;
	}
	public void setOptions(ArrayList<ProductOption> options) {
		this.options = options;
	}
	public ArrayList<ProductImage> getImages() {
		return images;
	}
	public void setImages(ArrayList<ProductImage> images) {
		this.images = images;
	}
	public ArrayList<ProductReview> getReviews() {
		return reviews;
	}
	public void setReviews(ArrayList<ProductReview> reviews) {
		this.reviews = reviews;
	}
	
}
