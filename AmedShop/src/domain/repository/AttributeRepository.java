package domain.repository;

import core.AbstractRepository;

public class AttributeRepository extends AbstractRepository {
	
	protected static AbstractRepository instance;
	
	public AttributeRepository() throws Exception {
		super();
	}

	public static AttributeRepository getInstance() throws Exception{
		if(instance == null){
			instance = new AttributeRepository();
		}
		return (AttributeRepository) instance;
	}

}
