package domain.repository;

import core.AbstractRepository;

public class CategoryRepository extends AbstractRepository {
	
	protected static AbstractRepository instance;
	
	public CategoryRepository() throws Exception {
		super();
	}

	public static CategoryRepository getInstance() throws Exception{
		if(instance == null){
			instance = new CategoryRepository();
		}
		return (CategoryRepository) instance;
	}

}
