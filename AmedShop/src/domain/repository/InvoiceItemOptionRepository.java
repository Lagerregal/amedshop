package domain.repository;

import core.AbstractRepository;

public class InvoiceItemOptionRepository extends AbstractRepository {
	
	protected static AbstractRepository instance;
	
	public InvoiceItemOptionRepository() throws Exception {
		super();
	}

	public static InvoiceItemOptionRepository getInstance() throws Exception{
		if(instance == null){
			instance = new InvoiceItemOptionRepository();
		}
		return (InvoiceItemOptionRepository) instance;
	}

}
