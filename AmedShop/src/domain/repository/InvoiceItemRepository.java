package domain.repository;

import core.AbstractRepository;

public class InvoiceItemRepository extends AbstractRepository {
	
	protected static AbstractRepository instance;
	
	public InvoiceItemRepository() throws Exception {
		super();
	}

	public static InvoiceItemRepository getInstance() throws Exception{
		if(instance == null){
			instance = new InvoiceItemRepository();
		}
		return (InvoiceItemRepository) instance;
	}

}
