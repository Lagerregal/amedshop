package domain.repository;

import core.AbstractRepository;

public class InvoiceRepository extends AbstractRepository {
	
	protected static AbstractRepository instance;
	
	public InvoiceRepository() throws Exception {
		super();
	}

	public static InvoiceRepository getInstance() throws Exception{
		if(instance == null){
			instance = new InvoiceRepository();
		}
		return (InvoiceRepository) instance;
	}

}
