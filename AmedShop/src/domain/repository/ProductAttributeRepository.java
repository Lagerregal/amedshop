package domain.repository;

import core.AbstractRepository;

public class ProductAttributeRepository extends AbstractRepository {
	
	protected static AbstractRepository instance;
	
	public ProductAttributeRepository() throws Exception {
		super();
	}

	public static ProductAttributeRepository getInstance() throws Exception{
		if(instance == null){
			instance = new ProductAttributeRepository();
		}
		return (ProductAttributeRepository) instance;
	}

}
