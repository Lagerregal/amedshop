package domain.repository;

import core.AbstractRepository;

public class ProductImageRepository extends AbstractRepository {
	
	protected static AbstractRepository instance;
	
	public ProductImageRepository() throws Exception {
		super();
	}

	public static ProductImageRepository getInstance() throws Exception{
		if(instance == null){
			instance = new ProductImageRepository();
		}
		return (ProductImageRepository) instance;
	}

}
