package domain.repository;

import core.AbstractRepository;

public class ProductOptionRepository extends AbstractRepository {
	
	protected static AbstractRepository instance;
	
	public ProductOptionRepository() throws Exception {
		super();
	}

	public static ProductOptionRepository getInstance() throws Exception{
		if(instance == null){
			instance = new ProductOptionRepository();
		}
		return (ProductOptionRepository) instance;
	}

}
