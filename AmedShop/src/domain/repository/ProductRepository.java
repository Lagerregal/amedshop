package domain.repository;

import core.AbstractRepository;

public class ProductRepository extends AbstractRepository {
	
	protected static AbstractRepository instance;
	
	public ProductRepository() throws Exception {
		super();
	}

	public static ProductRepository getInstance() throws Exception{
		if(instance == null){
			instance = new ProductRepository();
		}
		return (ProductRepository) instance;
	}

}
