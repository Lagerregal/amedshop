package domain.repository;

import core.AbstractRepository;

public class ProductReviewRepository extends AbstractRepository {
	
	protected static AbstractRepository instance;
	
	public ProductReviewRepository() throws Exception {
		super();
	}

	public static ProductReviewRepository getInstance() throws Exception{
		if(instance == null){
			instance = new ProductReviewRepository();
		}
		return (ProductReviewRepository) instance;
	}

}
