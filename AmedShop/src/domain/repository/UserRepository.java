package domain.repository;

import core.AbstractRepository;

public class UserRepository extends AbstractRepository {
	
	protected static AbstractRepository instance;
	
	public UserRepository() throws Exception {
		super();
	}

	public static UserRepository getInstance() throws Exception{
		if(instance == null){
			instance = new UserRepository();
		}
		return (UserRepository) instance;
	}

}
