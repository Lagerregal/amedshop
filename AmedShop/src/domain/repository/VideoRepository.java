package domain.repository;

import core.AbstractRepository;

public class VideoRepository extends AbstractRepository {
	
	protected static AbstractRepository instance;
	
	public VideoRepository() throws Exception {
		super();
	}

	public static VideoRepository getInstance() throws Exception{
		if(instance == null){
			instance = new VideoRepository();
		}
		return (VideoRepository) instance;
	}

}
