package util;

import java.io.IOException;
import java.security.MessageDigest;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.model.User;
import domain.model.Video;

import view.Template;

public class GeneralUtility {
	
	protected static User user;

	
	/**
	 * ucFirst
	 * 
	 * @param text
	 * @return
	 */
	public static String ucFirst(String text){
		return text.substring(0,1).toUpperCase() + text.substring(1);
	}
	

	/**
	 * forwardController
	 * 
	 * @param sc
	 * @param request
	 * @param response
	 * @param controller
	 * @throws ServletException
	 * @throws IOException
	 */
	public static void forwardController(ServletContext sc, HttpServletRequest request, HttpServletResponse response, String controller){
		try{
			sc.getRequestDispatcher("/" + GeneralUtility.ucFirst(controller) + "Controller").include(request, response);
		}catch(Exception e){
			sc.setAttribute("exception", e);
			GeneralUtility.forwardController(sc, request, response, "error");
		}
	}
	
	
	/**
	 * getTemplate
	 * 
	 * @param sc
	 * @return
	 */
	public static Template getTemplate(ServletContext sc){
		return ((Template) sc.getAttribute("template"));
	}
	
	
	/**
	 * getHash
	 * 
	 * @param text
	 * @return
	 */
	public static String getHash(String text){
		StringBuffer hexString = new StringBuffer();
		
		try {
			byte[] bytesOfMessage = text.getBytes("UTF-8");
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] thedigest = md.digest(bytesOfMessage);
						
			for (int i=0; i<thedigest.length; i++) {
				if(thedigest[i] <= 15 && thedigest[i] >= 0){
					hexString.append("0");
				}
				hexString.append(Integer.toHexString(0xFF & thedigest[i]));
			}
		} catch (Exception e) { }
		
        return hexString.toString();
	}


	public static User getUser() {
		return user;
	}


	public static void setUser(User user) {
		GeneralUtility.user = user;
	}
	
	
	/**
	 * replaceMediaMarkers
	 * 
	 * @param text
	 * @return
	 * @throws Exception
	 */
	/*
	public static String replaceMediaMarkers(String text) throws Exception{
		while(text.indexOf("{video:") != -1){
			int startPos = text.indexOf("{video:");
			int endPos = text.indexOf("}", startPos+1);
			Video vid = VideoRepository.getInstance().findByUid(Integer.parseInt(text.substring(startPos+7, endPos)));
			
			text = text.substring(0, startPos) + getVideoCode(vid) + text.substring(endPos+1);
		}
		while(text.indexOf("{img:") != -1){
			int startPos = text.indexOf("{img:");
			int endPos = text.indexOf("}", startPos+1);
			Image img = ImageRepository.getInstance().findByUid(Integer.parseInt(text.substring(startPos+5, endPos)));
			
			text = text.substring(0, startPos) + getImageCode(img) + text.substring(endPos+1);
		}
		return text;
	}
	*/
	
	
	/**
	 * getVideoCode
	 * 
	 * @param vid
	 * @return
	 */
	public static String getVideoCode(Video vid){
		String code = "";
		if(vid.getSource().equals("youtube")){
			code = "<object width='840' height='472'>" +
					"<param name='movie' value='//www.youtube.com/v/"+vid.getSourceId()+"?hl=de&amp;version=3&amp;rel=0'></param>" +
					"<param name='allowFullScreen' value='true'></param>" +
					"<param name='allowscriptaccess' value='always'></param>" +
					"<embed src='//www.youtube.com/v/"+vid.getSourceId()+"?hl=en_US&amp;version=3&amp;rel=0&amp;iv_load_policy=3&amp;autoplay=1' type='application/x-shockwave-flash' width='840' height='472' allowscriptaccess='always' allowfullscreen='true'></embed>" +
					"</object>";
		}
		return code;
	}

}
