package util;

import domain.model.*;

public class InvoiceUtility {

	public static double calcTotalPrice(Invoice invoice){
		double total = 0;
		
		for(InvoiceItem item : invoice.getItems()){
			total += item.getAmount() * item.getPriceBase();
		}
		
		return total;
	}
}
