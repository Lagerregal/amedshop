package util;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

public class Mail {
	protected static final String API_URL = "http://service.markus-hoelzle.de/api/mail.php";

	protected String to;
	protected String from;
	protected String subject;
	protected String message;
	protected String apiKey;
	protected String attachmentFilePath;
	protected String attachmentFileType;
	protected String attachmentFileName;
	
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getApiKey() {
		return apiKey;
	}
	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}
	public String getAttachmentFilePath() {
		return attachmentFilePath;
	}
	public void setAttachmentFilePath(String attachmentFilePath) {
		this.attachmentFilePath = attachmentFilePath;
	}
	public String getAttachmentFileType() {
		return attachmentFileType;
	}
	public void setAttachmentFileType(String attachmentFileType) {
		this.attachmentFileType = attachmentFileType;
	}
	public String getAttachmentFileName() {
		return attachmentFileName;
	}
	public void setAttachmentFileName(String attachmentFileName) {
		this.attachmentFileName = attachmentFileName;
	}
	
	
	public void send() throws Exception{
		String urlToConnect = API_URL
				+ "?to=" + URLEncoder.encode(getTo(), "UTF-8")
				+ "&from=" + URLEncoder.encode(getFrom(), "UTF-8")
				+ "&subject=" + URLEncoder.encode(getSubject(), "UTF-8")
				+ "&message=" + URLEncoder.encode(getMessage(), "UTF-8")
				+ "&key=" + getApiKey();
		String boundary = Long.toHexString(System.currentTimeMillis()); // Just generate some unique random value.

		URLConnection connection = new URL(urlToConnect).openConnection();
		connection.setDoOutput(true); // This sets request method to POST.
		connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
		BufferedOutputStream writer = null;
		try {
		    writer = new BufferedOutputStream(connection.getOutputStream());
		    writer.write(("--" + boundary + "\n").getBytes("UTF-8"));
		    writer.write(("Content-Disposition: form-data; name=\"file\"; filename=\""+getAttachmentFileName()+"\"\n").getBytes("UTF-8"));
		    writer.write(("Content-Type: "+getAttachmentFileType()+"; charset=UTF-8\n").getBytes("UTF-8"));
		    writer.write("\n".getBytes("UTF-8"));
		    byte[] data = java.nio.file.Files.readAllBytes(java.nio.file.Paths.get(getAttachmentFilePath()));
		    writer.write(data);
		    writer.write(("--" + boundary + "--\n").getBytes("UTF-8"));
		} finally {
		    if (writer != null) writer.close();
		}
		
		//read response to println
		BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		String inputLine;
		while ((inputLine = in.readLine()) != null){
			System.out.println(inputLine);
		}
		in.close();
	}
}
