package view;

import java.io.IOException;
import java.util.Map;

import domain.model.Category;
import domain.model.Product;
import domain.repository.CategoryRepository;
import domain.repository.ProductRepository;
import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;

public class BreadcrumpDirective implements TemplateDirectiveModel {

	@SuppressWarnings("rawtypes")
	public void execute(Environment env, Map params, TemplateModel[] loopVars, TemplateDirectiveBody body) throws TemplateException, IOException {
		StringBuffer returnString = new StringBuffer();
		Product product = null;
		Category childCategory = null;
		Category currentCategory = null;
		boolean isLast = true;
		
		try {
	        if(params.get("product") != null){        	
				
					product = ProductRepository.getInstance().findByUid(params.get("product").toString());
					childCategory = CategoryRepository.getInstance().findByUid(product.getCategory());
					returnString.append("<li class='active'>" + product.getTitle() + "</li>");	
					isLast = false;
	        }
	        if(product == null){
				childCategory = CategoryRepository.getInstance().findByUid(params.get("category").toString());
	        }
	        currentCategory = childCategory;
	        while(currentCategory != null){
	        	if(isLast){
	        		returnString.insert(0, "<li class='active'>" + currentCategory.getTitle() + "</li>");
	        	}else{
	        		returnString.insert(0, "<li><a href='IndexController?event=catalog&category="+ currentCategory.getUid() +"'>"+ currentCategory.getTitle() +"</a></li>");
	        	}
	        	currentCategory = currentCategory.getParentCategory();
	        	isLast = false;
	        }
                
        } catch (Exception e) {
			e.printStackTrace();
		}
        env.getOut().append(returnString.toString());
    }

}
