package view;

import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Map;

import freemarker.core.Environment;
import freemarker.template.SimpleNumber;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;

public class DateFormatDirective implements TemplateDirectiveModel {

	@SuppressWarnings("rawtypes")
	public void execute(Environment env,
            Map params, TemplateModel[] loopVars,
            TemplateDirectiveBody body)
            throws TemplateException, IOException {
        // Check if no parameters were given:
        if (!params.containsKey("date")) {
            throw new TemplateModelException(
                    "Price parameter needed");
        }
        if (loopVars.length != 0) {
                throw new TemplateModelException(
                    "This directive doesn't allow loop variables.");
        }
            
        try{
        	env.getOut().append(formatDate(((SimpleNumber) params.get("date")).getAsNumber().intValue()));
        }catch(Exception e){ e.printStackTrace(); }
    }
	
	public static String formatDate(int date){
		Calendar cal = GregorianCalendar.getInstance();
		cal.setTimeInMillis((long)date * (long)1000);
		return cal.get(Calendar.DAY_OF_MONTH) + "." + (cal.get(Calendar.MONTH)+1) + "." + cal.get(Calendar.YEAR);
	}

}
