package view;

import java.io.IOException;
import java.util.Map;

import freemarker.core.Environment;
import freemarker.template.SimpleNumber;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;

public class PriceFormatDirective implements TemplateDirectiveModel {

	@SuppressWarnings("rawtypes")
	public void execute(Environment env,
            Map params, TemplateModel[] loopVars,
            TemplateDirectiveBody body)
            throws TemplateException, IOException {
        // Check if no parameters were given:
        if (!params.containsKey("price")) {
            throw new TemplateModelException(
                    "Price parameter needed");
        }
        if (loopVars.length != 0) {
                throw new TemplateModelException(
                    "This directive doesn't allow loop variables.");
        }
            
        try{
        	env.getOut().append(formatPrice(((SimpleNumber) params.get("price")).getAsNumber().doubleValue()));
        }catch(Exception e){ e.printStackTrace(); }
    }
	
	public static String formatPrice(double price){
		return String.format("%1.2f", price) + " €";
	}

}
