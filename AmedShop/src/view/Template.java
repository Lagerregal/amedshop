package view;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import util.GeneralUtility;
import domain.model.*;
import domain.repository.*;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.TemplateExceptionHandler;

/**
 * A little template engine
 * 
 * @author Markus Hölzle
 */
public class Template {

	protected String body = new String();
	protected String title = new String();
	protected Map<String, String> vars = new HashMap<String, String>();
	protected HttpServletResponse response;
	protected ServletContext servletContext;	
	protected Map<String, Object> assign = new HashMap<String, Object>();
	protected String templateFile = "index";
	
	
	public Template(HttpServletResponse response, ServletContext sc){
		this.response = response;
		this.servletContext = sc;
		
		this.vars.put("logoText", "Nerd Shop");
	}
	
	
	public String getTitle() {
		return title;
	}

	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public Map<String, Object> getAssign() {
		return assign;
	}


	public void setAssign(Map<String, Object> assign) {
		this.assign = assign;
	}


	public String getTemplateFile() {
		String returnValue = templateFile;
		File file = new File(this.servletContext.getRealPath("templates") + "/" + returnValue + ".html");
        if(!file.canRead()){
        	returnValue = "index";
        }
		return returnValue;
	}


	public void setTemplateFile(String templateFile) {
		this.templateFile = templateFile;
	}


	/**
	 * Appends HTML to the document body
	 * 
	 * @param newBody Body part to append
	 * @return void
	 * @throws Exception 
	 */
	public void appendToBody(String newBody) throws Exception{
		//this.body += GeneralUtility.replaceMediaMarkers(newBody);
		this.body += newBody;
	}	
	
	
	/**
	 * Returns a HTML string ready to send
	 * 
	 * @return String HTML
	 * @throws Exception 
	 */
	public String getHtml() throws Exception{
		String html = "";		
		BufferedReader br = new BufferedReader(new FileReader(this.servletContext.getRealPath("main.html")));
		
		for ( String line; (line = br.readLine()) != null; ){
			html += line;
	    }
		
	    br.close();
		return html;
	}
	
	
	/**
	 * Replace all markers with value
	 * 
	 * @param html
	 * @return
	 * @throws Exception 
	 */
	public String replaceMarkers(String html) throws Exception{
		defineStaticMarkers();

		Set<String> keys = this.vars.keySet();	    
		for (String singleKey : keys) {
			html = html.replace("{"+singleKey+"}", this.vars.get(singleKey));
		}
		
		return html;
	}
	
	
	/**
	 * defineStaticMarkers
	 * 
	 * @throws Exception
	 */
	protected void defineStaticMarkers() throws Exception{		
		//Navigation left
		StringBuffer subMenu = new StringBuffer();
		ArrayList<Category> mainCategories = CategoryRepository.getInstance().findByField("parentCategory", "0");
		for(int i=0; i < mainCategories.size(); i++){
			subMenu.append("<a href='IndexController?event=catalog&category="+mainCategories.get(i).getUid()+"' class='list-group-item'>"+mainCategories.get(i).getTitle()+"</a>");
			ArrayList<Category> subCategories = CategoryRepository.getInstance().findByField("parentCategory", mainCategories.get(i).getUid()+"");
			for(int j=0; j<subCategories.size(); j++){
				subMenu.append("<a href='IndexController?event=catalog&category="+subCategories.get(j).getUid()+"' class='list-group-item' style='padding-left: 35px;'>"+subCategories.get(j).getTitle()+"</a>");
			}
		}
		this.vars.put("subMenu", subMenu.toString());
		
		
		//Main Navigation
		StringBuffer mainNav = new StringBuffer();
		User user = GeneralUtility.getUser();
		mainNav.append("<li><a href='IndexController?event=cart'>Warenkorb</a></li>" +
				"<!--<li><a href='IndexController?event=help'>Hilfe</a></li>-->");
		if(user == null){
			mainNav.append("<li><a href='IndexController?event=login'>Login</a></li>"
					+ "<li><a class='last' href='IndexController?event=register'>Registrieren</a></li>");
		}
		if(user != null){
			mainNav.append("<li><a href='IndexController?event=account'>Mein Konto</a></li>" +
					"<li><a class='last' href='IndexController?event=logout'>Logout</a></li>");
		}
		//mainNav.append("<li class='last'><a href='IndexController?event=impressum'>Impressum</a></li>");
		this.vars.put("mainNav", mainNav.toString());
	}
	
	
	/**
	 * Print the HTML
	 * 
	 * @return void
	 * @throws Exception 
	 */
	public void printHtml() throws Exception{
		this.response.setCharacterEncoding("UTF-8");
		this.response.setContentType("text/html");
		assign.put("bodytext", this.body);
		assign.put("title", title);
		PrintWriter writer;
		Writer stringWriter = new StringWriter();
		
		try {
			// freemarker.org
			Configuration cfg = new Configuration();
	        cfg.setDirectoryForTemplateLoading(new File(this.servletContext.getRealPath("templates")));
	        cfg.setObjectWrapper(new DefaultObjectWrapper());
	        cfg.setDefaultEncoding("UTF-8");
	        cfg.setTemplateExceptionHandler(TemplateExceptionHandler.HTML_DEBUG_HANDLER);
	        cfg.setSharedVariable("priceFormat", new PriceFormatDirective());
	        cfg.setSharedVariable("breadcrump", new BreadcrumpDirective());
	        cfg.setSharedVariable("dateFormat", new DateFormatDirective());
	        freemarker.template.Template temp = cfg.getTemplate(this.getTemplateFile() + ".html");
	        temp.process(assign, stringWriter);
	        
	        // output
	        this.vars.put("bodytext", stringWriter.toString());
		
			writer = this.response.getWriter();
			writer.print(this.replaceMarkers(this.getHtml()));
		} catch (Exception e) {
			this.response.getWriter().println("<div style='color: #FF0000; width: 1300; margin: 0 auto;'>Fehler im Template:<br /></div>"
					+ "<pre style='width: 1300px; word-wrap: break-word; margin: 0 auto; font-size: 18px; font-weight: bold;'>");
			e.printStackTrace(this.response.getWriter());
			this.response.getWriter().println("</pre>");
		}
	}
	
}
